/* global tabKit uiKit settingsStore */
// thottery regex for domain name detection
// b/c standard is a moron:
// eslint-disable-next-line
var domainPattern = /^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/

function redirect (url) {
  // test if it's a valid URL or not
  switch (url.substr(0, 3) !== 'htt') {
    case true:
      console.log('redirect says not a valid url')
      url = makeValidURL(document.getElementById('omnibox').value)
      break
    case false:
      console.log('redirect says is a valid url')
      break
  }
  tabKit.navigation(tabKit.currentActiveTab(), 'loadURL', url)
}

// if it has just a domain name and/or a slash, we need to stick a protocol in front of it so the webview can load it
function makeValidURL (url) {
  console.log('made ' + url + ' a valid URL')
  return 'http://' + url
}

function search (string) {
  redirect(settingsStore.get('searchProvider') + string)
}

function checkIfUrl (string) {
  if (/^https?:\/\//.test(string)) {
    string = /^https?:\/\/(.*)/.exec(string)[1]
  }
  switch (string.indexOf('/') > -1) {
    case true:
      console.log('it has a slash')
      string = string.split('/', 1)
      break
    case false:
      console.log('no slash')
      break
  }
  // test if it's a domain name or not
  return domainPattern.test(string)
}

// check for keypress
document.getElementById('omnibox').addEventListener('keypress', function (e) {
  var key = e.which || e.keyCode
  switch (key) {
    case 13:
      uiKit.toggleWebHub()
      console.log('key down!')
      if (document.getElementById('omnibox').value.substr(0, 11) === 'tram-pub://' || document.getElementById('omnibox').value.substr(0, 11) === 'tram-prv://') {
        tabKit.navigation(tabKit.currentActiveTab(), 'loadURL', document.getElementById('omnibox').value)
        return
      }
      switch (checkIfUrl(document.getElementById('omnibox').value)) {
        case true:
          console.log('true, redirecting')
          redirect(document.getElementById('omnibox').value)
          break
        case false:
          console.log('false, searching')
          search(document.getElementById('omnibox').value)
          break
      }
  }
})
