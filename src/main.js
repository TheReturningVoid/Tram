/* global secureCode */
process.title = 'Tram Main Process'
let electron = require('electron')
let app = electron.app
let BrowserWindow = electron.BrowserWindow
let os = require('os')
let projInfo = require('./projInfo.json')
let logKit = require('./logKit/index')
let requestingModule = 'main'
let tabID = 'N/A'
let activity = 'Management of application'
logKit(requestingModule, tabID, activity, 'info', 'App Launched')

let Store = require('electron-store')
let settingsStore = new Store({name: 'settings'})
let autoConfigStore = new Store({name: 'autoConfigStore'})

var URLToLoad
var windowWidth
var windowHeight

switch (settingsStore.get('hasBeenSetUp')) {
  case true:
    URLToLoad = `file://${__dirname}/index.html`
    switch (autoConfigStore.get('windowWidth')) {
      case undefined:
        windowWidth = 1500
        windowHeight = 960
        break
      default:
        windowWidth = autoConfigStore.get('windowWidth')
        windowHeight = autoConfigStore.get('windowHeight')
        break
    }
    break
  case undefined:
    URLToLoad = `file://${__dirname}/setup/index.html`
    windowWidth = 1100
    windowHeight = 700
    break
}

function randomString (length, chars) {
  var result = ''
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)]
  return result
}

global.secureCode = randomString(1024, '`1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')

logKit(requestingModule, tabID, activity, 'info', 'Platform details: ' + os.arch() + ' ' + os.type() + ' ' + os.platform() + ' ' + os.release())
logKit(requestingModule, tabID, activity, 'info', 'Application details: ' + projInfo.vendor + ' ' + projInfo.name + ' ' + projInfo.release + ' ' + projInfo.codename + ' ' + projInfo.version)

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
switch (require('electron-squirrel-startup')) {
  case true:
    app.quit()
    break
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: windowWidth,
    height: windowHeight,
    title: 'Tram',
    icon: require('path').join(__dirname, 'resources/icons/icon.png'),
    frame: false,
    backgroundColor: '#000000',
    show: false
  })
  logKit(requestingModule, tabID, activity, 'info', 'Created browser window object')

  // and load the index.html of the app.
  mainWindow.loadURL(URLToLoad)
  logKit(requestingModule, tabID, activity, 'info', 'Opened index.html (loaded chrome)')

  // Open the DevTools.
  mainWindow.webContents.openDevTools()
  logKit(requestingModule, tabID, activity, 'warn', 'Opened DevTools for chrome, remove before release')

  switch (autoConfigStore.get('windowPosX') === undefined) {
    case true:
      break

    case false:
      mainWindow.setPosition(autoConfigStore.get('windowPosX'), autoConfigStore.get('windowPosY'), false)
      break
  }

  mainWindow.once('ready-to-show', () => {
    logKit(requestingModule, tabID, activity, 'info', 'Ready to show!')
    mainWindow.show()
  })

  mainWindow.on('close', () => {
    console.log(mainWindow.getSize())
    logKit(requestingModule, tabID, activity, 'info', 'Window is closing, saving window size & position')
    switch (mainWindow.webContents.getURL() !== `file://${__dirname}/setup/index.html`) {
      case true:
        autoConfigStore.set('windowWidth', mainWindow.getSize()[0])
        autoConfigStore.set('windowHeight', mainWindow.getSize()[1])
        autoConfigStore.set('windowPosX', mainWindow.getPosition()[0])
        autoConfigStore.set('windowPosY', mainWindow.getPosition()[1])
        break
    }
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    logKit(requestingModule, tabID, activity, 'info', 'Window closed')
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.

app.on('ready', createWindow)
logKit(requestingModule, tabID, activity, 'info', 'Created browser window')

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  switch (process.platform !== 'darwin') {
    case true:
      logKit(requestingModule, tabID, activity, 'info', 'Quitting Tram')
      app.quit()
      break
  }
})

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  switch (mainWindow === null) {
    case true:
      createWindow()
      break
  }
})

// remove menubar
app.on('browser-window-created', function (e, window) {
  window.setMenu(null)
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
global.tabKit = require('./tabKit/index.js')
let path = require('path')

app.on('ready', () => {
  electron.protocol.registerFileProtocol('tram-pub', (request, callback) => {
    var url = request.url.substr(11) // cover tram-pub://

    // eslint-disable-next-line
    var safeSuffix = path.normalize(url).replace(/^(\.\.[\/\\])+/, '')
    var safeJoin = path.join(`${__dirname}/hostedResources/pub/`, safeSuffix)

    if (safeJoin.substr(-1) === '/') {
      safeJoin = safeJoin + 'index.html'
    }

    if (safeJoin.substring(safeJoin.lastIndexOf('.') + 1) === safeJoin) {
      safeJoin = safeJoin + '/index.html'
    }
    // because the linter is an idiot:
    // eslint-disable-next-line
    callback({path: path.normalize(safeJoin)})
  }, (error) => {
    if (error) console.error('Failed to register protocol')
  })

  electron.protocol.registerFileProtocol('tram-prv', (request, callback) => {
    var url = request.url.substr(11) // cover tram-prv://

    if (url.substr(0, 1025) === secureCode + '/') {
      url = url.substr(1025)
      // eslint-disable-next-line
      var safeSuffix = path.normalize(url).replace(/^(\.\.[\/\\])+/, '')
      var safeJoin = path.join(`${__dirname}/hostedResources/prv/`, safeSuffix)

      if (safeJoin.substr(-1) === '/') {
        safeJoin = safeJoin + 'index.html'
        console.log('check1')
      }

      if (safeJoin.substring(safeJoin.lastIndexOf('.') + 1) === safeJoin) {
        safeJoin = safeJoin + '/index.html'
      }
      // because the linter is an idiot:
      // eslint-disable-next-line
      callback({path: path.normalize(safeJoin)})
    }
  }, (error) => {
    if (error) console.error('Failed to register protocol')
  })
})
