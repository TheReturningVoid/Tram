let Store = require('electron-store')
let settingsStore = new Store({name: 'settings'})

function updateColors (colorName) {
  switch (colorName) {
    case 'Confetti':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#E83151')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#5E1321')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#F4AFB7')
      break

    case 'Dreamsicle':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#F8A100')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#624100')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#FED79C')
      break

    case 'Bittersweet':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#FF765E')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#632E28')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#FFCABE')
      break

    case 'Bondi':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#009FB7')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#003F4B')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#9CDBE1')
      break

    case 'Jade':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#2db78b')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#0F4B37')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#ABE1D3')
      break

    case 'Clover':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#29bf12')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#114D06')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#A7E3A2')
      break

    case 'Hydrant':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#C5132B')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#4D0713')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#E9A3A9')
      break

    case 'Lemon':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#F7D425')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#61560D')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#FDECA9')
      break

    case 'Tangerine':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#FF5432')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#632414')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#FFBAB0')
      break

    case 'Jam':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#A4036F')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#44032D')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#DA99C3')
      break

    case 'Cobalt':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#2F4FC8')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#111F50')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#ADBBEC')
      break

    case 'Bubblegum':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#ff1481')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#630833')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#FFA4CF')
      break

    case 'UFO':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#0cce6b')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#065029')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#9CECC5')
      break

    case 'Funk':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#a253db')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#422357')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#D8B9F3')
      break

    case 'Indy':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#6969b3')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#272747')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#C3C3E3')
      break

    case 'Vermillion':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#c3423f')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#4B181B')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#E7B4B1')
      break

    case 'Brick':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#DA6A63')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#562827')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#F2C4C3')
      break

    case 'Caramel':
      document.getElementsByTagName('html')[0].style.setProperty('--main-color', '#D07934')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-bg-color', '#523116')
      document.getElementsByTagName('html')[0].style.setProperty('--main-highlight-text-color', '#EEC7AC')
      break
  }
}

function updateBgColors (bgColor) {
  switch (bgColor) {
    case 'black':
      document.getElementsByTagName('html')[0].style.setProperty('--background-color', '#000000')
      document.getElementsByTagName('html')[0].style.setProperty('--foreground-color', '#EEEEEE')
      break

    case 'white':
      document.getElementsByTagName('html')[0].style.setProperty('--background-color', '#EEEEEE')
      document.getElementsByTagName('html')[0].style.setProperty('--foreground-color', '#000000')
      break
  }
}

function saveSetting (name, value) {
  settingsStore.set(name, value)
}

updateColors(settingsStore.get('accentColor'))
updateBgColors(settingsStore.get('bgColor'))

if (window.location.href === 'tram-prv://' + require('electron').remote.getGlobal('secureCode') + '/settings') {
  document.getElementsByClassName('column')[0].addEventListener('click', function (e) {
    window.location = 'tram-prv://' + require('electron').remote.getGlobal('secureCode') + '/settings/search.html'
  })

  document.getElementsByClassName('column')[1].addEventListener('click', function (e) {
    window.location.replace('tram-prv://' + require('electron').remote.getGlobal('secureCode') + '/settings/colors.html')
  })

  document.getElementsByClassName('column')[2].addEventListener('click', function (e) {
    window.location.replace('tram-prv://' + require('electron').remote.getGlobal('secureCode') + '/settings/reset.html')
  })

  document.getElementsByClassName('column')[3].addEventListener('click', function (e) {
    window.location.replace('tram-prv://' + require('electron').remote.getGlobal('secureCode') + '/settings/sources.html')
  })
} else {
  document.getElementById('dynamicButton').removeAttribute('onclick')
  document.getElementById('dynamicButton').setAttribute('onclick', "window.location.replace('tram-prv://" + require('electron').remote.getGlobal('secureCode') + "/settings')")
}
