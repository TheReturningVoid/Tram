/* global tabKit cache */
module.exports = function () {
  function createTab () {
    tabKit.create('tram-pub://webHubDummy', 'active')
    var activeTab = document.getElementsByClassName('active')[0].getAttribute('id')
    var tab = document.createElement('div')
    tab.innerHTML = '<h2>New tab</h2>'
    tab.className = 'tabCard tab'
    tab.style.background = '#0094FE'
    tab.id = 'tabid-' + activeTab
    document.getElementsByClassName('active')[0].addEventListener('did-finish-load', function () {
      tab.innerHTML = "<h2>" + document.getElementsByClassName('active')[0].getTitle() + "</h2>"
    })
    tab.setAttribute('data-tabid', activeTab)
    tab.onclick = function () {
      tabKit.changeFocus(tab.dataset.tabid, 'active')
    }
    document.getElementById('tabContainer').appendChild(tab)
  }
  function deleteTab (tabid) {
    var tabList = cache.get('tabList')
    var nearestTab
    if (document.getElementsByClassName('tab').length === '1') {
      console.log('There is only one tab')
    } else {
      // modified validityCheck to return true
      if (tabKit.validityCheck(tabList.indexOf(tabid) + 1)) {
        nearestTab = tabList.indexOf(tabid) + 1
      } else {
        nearestTab = tabList.indexOf(tabid) - 1
      }
      tabKit.changeFocus(cache.get('tabList')[nearestTab], 'active')
      document.getElementById('tabid-' + tabid).parentNode.removeChild(document.getElementById('tabid-' + tabid))
      tabKit.remove(tabid)
    }
  }
  document.getElementById('addTab').onclick = function () {
    createTab()
  }
  document.getElementById('closeTab').onclick = function () {
    deleteTab(document.getElementsByClassName('active')[0].getAttribute('id'))
  }
  createTab()
}
