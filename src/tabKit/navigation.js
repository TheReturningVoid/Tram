/* global logKit */

let requestingModule = 'tabKit.navigation'
module.exports = function (tabID, action, extra) {
  var tab = document.getElementById(tabID)
  switch (action) {
    case 'reload':
      tab.reload()
      logKit(requestingModule, tabID, action, 'info', 'Reloaded tab')
      break

    case 'reloadIgnoringCache':
      tab.reloadIgnoringCache()
      logKit(requestingModule, tabID, action, 'info', 'Reloaded tab ignoring cache')
      break

    case 'stop':
      tab.stop()
      logKit(requestingModule, tabID, action, 'info', 'Stopped navigation on tab')
      break

    case 'clearHistory':
      tab.clearHistory()
      logKit(requestingModule, tabID, action, 'info', 'Cleared history on tab')
      break

    case 'goBack':
      tab.goBack()
      logKit(requestingModule, tabID, action, 'info', 'Went back on tab')
      break

    case 'goForward':
      tab.goForward()
      logKit(requestingModule, tabID, action, 'info', 'Went forward on tab')
      break

    case 'loadURL':
      tab.loadURL(extra)
      break

    case 'forceLoadURL':
      tab.setAttribute('src', extra)
      break

    default:
      logKit(requestingModule, tabID, action, 'warn', 'Action/tabID unspecified or unrecognized! Expected "reload", "reloadIgnoringCache", "stop", "clearHistory", "goBack", or "goForward".')
      break
  }
}
