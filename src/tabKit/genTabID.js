/* global logKit cache */
let requestingModule = 'tabKit.genTabID'

// shoutouts to leverone (not simpleflips) for this black magic code - https://gist.github.com/LeverOne/1308368
function uuid (a, b) { for (b = a = ''; a++ < 36; b += a * 51 & 52 ? (a ^ 15 ? 8 ^ Math.random() * (a ^ 20 ? 16 : 4) : 4).toString(16) : '-');return b }

module.exports = function () {
  var tabList = cache.get('tabList')
  var internalTabID = uuid()

  switch (tabList.includes(internalTabID)) {
    case true:
      internalTabID = uuid()
      logKit(requestingModule, 'N/A', 'tabID Generation', 'warn', 'Matched existing tabID while generating new tabID')
      break
  }
  logKit(requestingModule, internalTabID, 'tabID Generation', 'info', 'Generated new tabID')
  tabList.push(internalTabID)
  cache.set('tabList', tabList)
  return internalTabID
}
