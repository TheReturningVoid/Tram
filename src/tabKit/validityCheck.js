/* global cache */
module.exports = function (tabNum) {
  var tabList = cache.get('tabList')
  if (typeof tabList[tabNum] === 'undefined') {
    return false
  }
}
