/* global logKit cache */
let requestingModule = 'tabKit.remove'
let action = 'Tab Removal'

module.exports = function (tabID) {
  var tabList = cache.get('tabList')
  function remove (element) {
    const index = tabList.indexOf(element)
    switch (index !== -1) {
      case true:
        tabList.splice(index, 1)
        break
    }
  }
  var tab = document.getElementById(tabID)
  tab.parentNode.removeChild(tab)
  remove(tabID)
  cache.set('tabList', tabList)
  cache.del('tabInfo-' + tabID)
  logKit(requestingModule, tabID, action, 'info', 'Removed this tab')
}
