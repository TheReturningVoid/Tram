/* global tabKit logKit cache */

let requestingModule = 'init'
let tabID = 'N/A'
let activity = 'Startup'

module.exports = function () {
  // initialize tabList array
  var obj = []
  cache.set('tabList', obj)

  // create first tab
  console.warn("This doesn't do much currently. Starting the create function.")
  logKit(requestingModule, tabID, activity, 'info', 'Creating first tab')
}
