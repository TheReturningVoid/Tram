let secureCode = require('electron').remote.getGlobal('secureCode')
console.log(secureCode)
let tabKit = require('./tabKit/index.js')
let logKit = require('./logKit/index.js')
let NodeCache = require('node-cache')
var cache = new NodeCache()

tabKit.init()
tabKit.tabUI()

let meta = require('./meta/index.js')
let projInfo = require('./projInfo.json')
let uiKit = require('./uiKit/index.js')

uiKit.buttonStalker()
uiKit.keyboardControls()

setTimeout(function () {
  uiKit.toggleWebHub()
}, 500)
logKit('main', 'N/A', 'Startup', 'info', 'Tram has started up!')
