const remote = require('electron').remote
var window = remote.getCurrentWindow()
var tabKit = require('../tabKit/index')
module.exports = function () {
  switch (window.isFullScreen()) {
    case true:
      window.setFullScreen(false)
      document.getElementById(tabKit.currentActiveTab()).classList.remove('tempFullScreen')
      break

    case false:
      window.setFullScreen(true)
      document.getElementById(tabKit.currentActiveTab()).classList.add('tempFullScreen')
      break
  }
}
