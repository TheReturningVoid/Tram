/* global tabKit uiKit */
const remote = require('electron').remote
module.exports = function () {
  document.getElementById('close').addEventListener('click', function (e) {
    var window = remote.getCurrentWindow()
    window.close()
  })

  document.getElementById('maximize').addEventListener('click', function (e) {
    var window = remote.getCurrentWindow()
    switch (window.isMaximized()) {
      case true:
        window.unmaximize()
        break
      case false:
        window.maximize()
        break
    }
  })

  document.getElementById('reload').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'reload')
  })

  document.getElementById('back').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'goBack')
  })

  document.getElementById('forward').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'goForward')
  })

  document.getElementById('settings').addEventListener('click', function (e) {
    uiKit.toggleSettingsPane()
  })

  document.getElementById('home').addEventListener('click', function (e) {
    uiKit.toggleWebHub()
  })
}
