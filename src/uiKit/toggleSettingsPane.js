/* global secureCode */
let projInfo = require('../projInfo.json')
module.exports = function () {
  var pane = document.createElement('webview')
  pane.setAttribute('src', 'tram-prv://' + secureCode + '/settings')
  pane.setAttribute('id', 'settingsPane')
  pane.setAttribute('nodeIntegration', 'true')
  document.getElementsByTagName('body')[0].appendChild(pane)
  pane = document.getElementById('settingsPane')
}
module.exports = function () {
  switch (document.getElementById('settingsPane') === null) {
    case true:
      var pane = document.createElement('webview')
      pane.setAttribute('src', 'tram-prv://' + secureCode + '/settings')
      pane.setAttribute('id', 'settingsPane')
      pane.setAttribute('nodeIntegration', 'true')
      pane.setAttribute('useragent', projInfo.vendor.replace(/ /g, '') + projInfo.name.replace(/ /g, '') + '/' + projInfo.release.replace(/ /g, '') + ' Mozilla/5.0 Chromium/' + process.versions['chrome'])
      document.getElementsByTagName('body')[0].appendChild(pane)

      var darkener = document.createElement('div')
      darkener.setAttribute('id', 'darkener')
      document.getElementsByTagName('body')[0].appendChild(darkener)

      document.getElementById('settingsPane').addEventListener('did-navigate', (e) => {
        if (e.url === 'tram-pub://close') {
          require('./index.js').toggleSettingsPane()
        }
      })
      break

    case false:
      document.getElementById('settingsPane').removeEventListener('did-navigate', (e) => {
        if (e.url === 'tram-pub://close') {
          require('./index.js').toggleSettingsPane()
        }
      })
      document.getElementsByTagName('body')[0].removeChild(document.getElementById('settingsPane'))
      document.getElementsByTagName('body')[0].removeChild(document.getElementById('darkener'))
      break
  }
}
